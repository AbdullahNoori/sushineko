# SushiNeko

Throughout this tutorial you will learn many concepts including how to:

Effectively use SpriteKit to rapidly prototype a game
Build an 'endless' core mechanic
Use property observers in Swift
Add effective animations
Add simple visual effects
Implement game state management
